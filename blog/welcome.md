---
title: Welcome to Hermades School
thumb: /assets/images/blog/welcome.jpeg
category: Information
author: hermades
footer: 1
date: 2021-01-06 19:20:25
layout: entry
---

Hello everybody, it's me Hermades. Thanks for visiting my website.

This is Hermades School, a completely free LMS platform aimed to help people to
learn programming, hacking and things related with computing. This is a free
(as in freedom) platform, which means you get the four essential freedoms:

* The freedom to run the program as wish, for any purpose. (freedom 0)
* The freedom to study how the program works, and change it so it does your
 computing as you wish. (freedom 1)
* The freedom to redistribute copies so you can help others. (freedom 2)
* The freedom to distribute your modified versions to others.(freedom 3)

That means you can modify this website and create your own platform based on
the HSchool code or contribute lessons, courses and improvements.

HSchool is and will always be free, there are no hidden fees, not even
registration or subscription posibilities, you can [donate](/donate) to keep the
project alive.

**Note**: The source code of HSChool is licensed under the [GPL 3.0 or
above](/LICENSE) license, make sure to read it before contributing any changes,
redistributing or create your own product based on HSchool.

The contents (courses, lessons and blog entries) are licensed under the
[CC-BY-ND 3.0](https://creativecommons.org/licenses/by-nd/3.0/) license, make
sure to also read it before doing any changes.

You can get more information about why HSchool is free and a little bit
of its history [here](/donate).

## What I will be working on?

Currently there are three courses:

1. Hacking From Scratch
2. Linux For Beginners
3. C For Beginners

For now, only one of them (_Hacking From Scratch_) will get updated, the others
two will be in standby because I will also have to work in HSchool, I
need to add more content than just lessons, I started working on this yesterday
which means there is still a lot of things pendant to do, such as searching,
filtering and that sort of stuff, I will work on those things as well with the
first course. Then, I will start working on the _C For Beginners_ course, and
once either of them is finished I will start working on the _Linux For
Beginners_ course.

Thank you for visiting and supporting HSchool.
